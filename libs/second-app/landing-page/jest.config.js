module.exports = {
  name: 'second-app-landing-page',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/second-app/landing-page',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
