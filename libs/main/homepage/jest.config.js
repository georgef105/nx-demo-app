module.exports = {
  name: 'main-homepage',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/main/homepage',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
