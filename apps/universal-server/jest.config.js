module.exports = {
  name: 'universal-server',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/universal-server'
};
