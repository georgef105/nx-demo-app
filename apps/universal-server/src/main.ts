import 'zone.js/dist/zone-node';
import { join } from 'path';
import { getApp } from './app';

const {
  AppServerModuleNgFactory,
  LAZY_MODULE_MAP
} = require('../../../dist/apps/main-universal/main');

const app = getApp({
  clientDistPath: join(__dirname, '../../../dist/apps/main'),
  moduleFactory: AppServerModuleNgFactory,
  moduleMap: LAZY_MODULE_MAP
});

const port = process.env.port || 3333;
app.listen(port, (err) => {
  if (err) {
    console.error(err);
  }
  console.log(`Listening at http://localhost:${port}`);
});
